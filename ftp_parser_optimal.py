import os
from ftplib import FTP
import zipfile
import pandas as pd
import xml.etree.ElementTree as ET
import tempfile
import logging

flag=0
k=0
i = 0
part = 84
skip_count = 0
logging.basicConfig(format='%(asctime)s %(message)s', level=logging.INFO, filename="py_log.log",filemode="w")

def download_and_extract_xml_files(ftp, remote_directory, local_directory, activity_df, max_rows_per_excel):
    try:
        ftp.cwd(remote_directory)
        dir_list = ftp.nlst()
        global flag
        for dir_name in dir_list:
            file_list = ftp.nlst(dir_name)
            logging.info('Current directory is '+dir_name)
            print(dir_name)
            for file_name in file_list:
                if file_name.endswith('.zip'):
                    logging.info('  Current zip-file is '+file_name)
                    with tempfile.TemporaryDirectory() as temp_dir:
                        local_zip_path = os.path.join(local_directory, os.path.basename(file_name.replace('/', '\\')))

                        with open(local_zip_path, 'wb') as local_file:
                            ftp.retrbinary('RETR ' + file_name, local_file.write)

                        with zipfile.ZipFile(local_zip_path, 'r') as zip_file:
                            for zip_info in zip_file.infolist():
                                if zip_info.filename.endswith('.xml'):
                                    xml_data = zip_file.read(zip_info.filename)
                                    xml_file_path = os.path.join(local_directory, zip_info.filename)
                                    with open(xml_file_path, 'wb') as xml_file:
                                        xml_file.write(xml_data)
                                        activity_df = parse_xml_file(xml_file_path, activity_df, max_rows_per_excel)
                                        logging.info('      Current xml-file is '+zip_info.filename)
                                    global i, k
                                    k=k+1

    except Exception as e:
        global part
        save_to_excel(activity_df, f'activity_result_{part}.xlsx')
        part=part+1
        logging.error(f"An error occurred: {str(e)}")
        print(f"An error occurred: {str(e)}")

    return activity_df

def parse_xml_file(xml_file_path, activity_df: pd.DataFrame, max_rows_per_excel):
    try:
        tree = ET.parse(xml_file_path)
        root = tree.getroot()

        for position in root.findall('.//{http://bus.gov.ru/external/1}position'):
            if position.find('.//{http://bus.gov.ru/types/1}placer') is not None:
                placer = position.find('.//{http://bus.gov.ru/types/1}placer')
            else:
                continue
            position_id = position.find('.//{http://bus.gov.ru/types/1}positionId').text if position.find('.//{http://bus.gov.ru/types/1}positionId') is not None else ''
            fullname = placer.find('.//{http://bus.gov.ru/types/1}fullName').text if placer.find('.//{http://bus.gov.ru/types/1}fullName') is not None else ''
            inn = placer.find('.//{http://bus.gov.ru/types/1}inn').text if placer.find('.//{http://bus.gov.ru/types/1}inn') is not None else ''
            kpp = placer.find('.//{http://bus.gov.ru/types/1}kpp').text if placer.find('.//{http://bus.gov.ru/types/1}kpp') is not None else ''
            report_year = position.find('.//{http://bus.gov.ru/types/1}reportYear').text if position.find('.//{http://bus.gov.ru/types/1}reportYear') is not None else ''
            staff = position.find('.//{http://bus.gov.ru/types/1}staff')
            begin_year = staff.find('.//{http://bus.gov.ru/types/1}beginYear').text if staff.find('.//{http://bus.gov.ru/types/1}beginYear') is not None else ''
            end_year = staff.find('.//{http://bus.gov.ru/types/1}endYear').text if staff.find('.//{http://bus.gov.ru/types/1}endYear') is not None else ''
            avg_salary = staff.find('.//{http://bus.gov.ru/types/1}averageSalary').text if staff.find('.//{http://bus.gov.ru/types/1}averageSalary') is not None else ''

            index = 0 if begin_year == '0' else float(end_year) / float(begin_year)

            if (0 <= index <= 0.10) or (index >= 21):
                comment = 'Большое различие!'
            elif 0.11 <= index <= 0.70:
                comment = 'Сокращение'
            elif 0.71 <= index <= 1.30:
                comment = 'Норм'
            elif 1.31 <= index < 21:
                comment = 'Прирост'
            else:
                comment = ''

            activity_df = pd.concat([activity_df, pd.DataFrame({'position_id':[position_id], 'inn': [inn],
                                            'kpp':[kpp], 'fullname':[fullname],
                                            'report_year': [report_year], 'begin_year': [begin_year],
                                            'end_year': [end_year], 'index': [index], 'comment': [comment],
                                            'avg_salary': [avg_salary]})], ignore_index=True)
            global k, part
            if k >= max_rows_per_excel:
                global part
                save_to_excel(activity_df, f'activity_result_{part}.xlsx')
                part =part+1
                k=0
                activity_df = pd.DataFrame(columns=['position_id', 'inn', 'kpp', 'report_year', 'begin_year', 'end_year', 'index', 'comment', 'avg_salary'])

    except Exception as e:
        logging.error(f"An error occurred while parsing {xml_file_path}: {str(e)}")

    return activity_df

def save_to_excel(activity_df:pd.DataFrame, excel_file_name):
    try:
        activity_df.to_excel(excel_file_name, index=False)
        logging.info(f'File {excel_file_name} saved succefull')
    except Exception as e:
        logging.error(f"An error occurred while saving to Excel: {str(e)}")
        print(f"An error occurred while saving to Excel: {str(e)}")

def main():
    global part
    # Параметры FTP-сервера
    ftp_host = 'ftp.bus.gov.ru'
    ftp_user = 'gmuext'
    ftp_pass = 'YctTa34AdOPyld2'
    remote_directory = 'ActivityResult'

    # Локальная директория для сохранения архивов и .xml файлов
    local_directory = 'ActivityResult_2'

    # Создание локальной директории, если она не существует
    if not os.path.exists(local_directory):
        os.makedirs(local_directory)

    # Создание пустого DataFrame для хранения данных
    activity_df = pd.DataFrame(columns=['position_id','inn', 'kpp', 'fullname', 'report_year', 'begin_year', 'end_year', 'index', 'comment', 'avg_salary'])

    # Максимальное количество строк в Excel файле
    max_rows_per_excel = 100000

    try:
        # Подключение к FTP-серверу
        with FTP(ftp_host) as ftp:
            ftp.login(user=ftp_user, passwd=ftp_pass)
            print('I`m in')
            # Скачивание и извлечение .xml файлов из архивов
            activity_df = download_and_extract_xml_files(ftp, remote_directory, local_directory, activity_df, max_rows_per_excel)

        # Сохранение оставшихся данных в Excel файл
        if not activity_df.empty:
            save_to_excel(activity_df, 'activity_result_final.xlsx')

    except KeyboardInterrupt:
        save_to_excel(activity_df, f'activity_result_{part+1}.xlsx')
    except Exception as e:
        save_to_excel(activity_df, f'activity_result_{part}.xlsx')
        print(f"An error occurred during FTP connection: {str(e)}")

if __name__ == "__main__":
    main()
