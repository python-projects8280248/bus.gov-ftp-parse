import os
from ftplib import FTP
import zipfile
import pandas as pd
import xml.etree.ElementTree as ET
import tempfile
import logging

k=0
i = 0
part = 0
skip_count = 0
logging.basicConfig(format='%(asctime)s %(message)s', level=logging.INFO, filename="py_companies_log.log",filemode="w")

def download_and_extract_xml_files(ftp, remote_directory, local_directory, companies_df, max_rows_per_excel):
    try:
        ftp.cwd(remote_directory)
        dir_list = ftp.nlst()
        for dir_name in dir_list:
            file_list = ftp.nlst(dir_name)
            logging.info('Current directory is '+dir_name)
            print(dir_name)
            for file_name in file_list:
                if file_name.endswith('.zip'):
                    logging.info('  Current zip-file is '+file_name)
                    with tempfile.TemporaryDirectory() as temp_dir:
                        local_zip_path = os.path.join(local_directory, os.path.basename(file_name.replace('/', '\\')))

                        with open(local_zip_path, 'wb') as local_file:
                            ftp.retrbinary('RETR ' + file_name, local_file.write)

                        with zipfile.ZipFile(local_zip_path, 'r') as zip_file:
                            for zip_info in zip_file.infolist():

                                if zip_info.filename.endswith('.xml'):
                                    xml_data = zip_file.read(zip_info.filename)
                                    xml_file_path = os.path.join(local_directory, zip_info.filename)
                                    with open(xml_file_path, 'wb') as xml_file:
                                        xml_file.write(xml_data)
                                        activity_df = parse_xml_file(xml_file_path, companies_df, max_rows_per_excel)
                                        logging.info('      Current xml-file is '+zip_info.filename)
                                    global i, k
                                    k=k+1

    except Exception as e:
        global part
        save_to_excel(companies_df, f'companies_info_{part}.xlsx')
        part=part+1
        logging.error(f"An error occurred: {str(e)}")
        print(f"An error occurred: {str(e)}")

    return activity_df

def parse_xml_file(xml_file_path, companies_df: pd.DataFrame, max_rows_per_excel):
    try:
        tree = ET.parse(xml_file_path)
        root = tree.getroot()

        for position in root.findall('.//{http://bus.gov.ru/external/1}position'):
            if position.find('.//{http://bus.gov.ru/types/1}placer') is not None:
                placer = position.find('.//{http://bus.gov.ru/types/1}placer')
            else:
                continue
            position_id = position.find('.//{http://bus.gov.ru/types/1}positionId').text if position.find('.//{http://bus.gov.ru/types/1}positionId') is not None else ''
            
            full_name = placer.find('{http://bus.gov.ru/types/1}fullName').text if placer.find('.//{http://bus.gov.ru/types/1}fullName') is not None else ''
            inn = placer.find('{http://bus.gov.ru/types/1}inn').text if placer.find('.//{http://bus.gov.ru/types/1}inn') is not None else ''
            initiator = position.find('.//{http://bus.gov.ru/types/1}initiator') if position.find('.//{http://bus.gov.ru/types/1}initiator') is not None else ''
            full_name_initiator = initiator.find('{http://bus.gov.ru/types/1}fullName').text if initiator.find('{http://bus.gov.ru/types/1}fullName') is not None else ''
            main = position.find('.//{http://bus.gov.ru/types/1}main') if position.find('.//{http://bus.gov.ru/types/1}main') is not None else ''
            short_name = main.find('{http://bus.gov.ru/types/1}shortName').text if main.find('{http://bus.gov.ru/types/1}shortName') is not None else ''
            classifier = main.find('{http://bus.gov.ru/types/1}classifier') if main.find('{http://bus.gov.ru/types/1}classifier') is not None else ''
            okfs = classifier.find('{http://bus.gov.ru/types/1}okfs') if classifier.find('{http://bus.gov.ru/types/1}okfs') is not None else ''
            okfs_name = okfs.find('{http://bus.gov.ru/types/1}name').text if okfs.find('{http://bus.gov.ru/types/1}name') is not None else ''
            okopf = classifier.find('{http://bus.gov.ru/types/1}okopf') if classifier.find('{http://bus.gov.ru/types/1}okopf') is not None else ''
            okopf_name = okopf.find('{http://bus.gov.ru/types/1}name').text if okopf.find('{http://bus.gov.ru/types/1}name') is not None else ''
            oktmo = classifier.find('{http://bus.gov.ru/types/1}oktmo') if classifier.find('{http://bus.gov.ru/types/1}oktmo') is not None else ''
            oktmo_name = oktmo.find('{http://bus.gov.ru/types/1}name').text if oktmo.find('{http://bus.gov.ru/types/1}name') is not None else ''
            additional = position.find('{http://bus.gov.ru/types/1}additional') if position.find('{http://bus.gov.ru/types/1}additional') is not None else ''
            instType = additional.find('{http://bus.gov.ru/types/1}institutionType') if additional.find('{http://bus.gov.ru/types/1}institutionType') is not None else ''
            type_name = instType.find('{http://bus.gov.ru/types/1}name').text if instType.find('{http://bus.gov.ru/types/1}name') is not None else ''
            www = additional.find('{http://bus.gov.ru/types/1}www').text if additional.find('{http://bus.gov.ru/types/1}www') is not None else ''
            eMail = additional.find('{http://bus.gov.ru/types/1}eMail').text if additional.find('{http://bus.gov.ru/types/1}eMail') is not None else ''
            phone = additional.find('{http://bus.gov.ru/types/1}phone').text if additional.find('{http://bus.gov.ru/types/1}phone') is not None else ''
            other = position.find('{http://bus.gov.ru/types/1}other') if position.find('{http://bus.gov.ru/types/1}other') is not None else ''
            chief = other.find('{http://bus.gov.ru/types/1}chief') if other.find('{http://bus.gov.ru/types/1}chief') is not None else ''
            last_name = chief.find('{http://bus.gov.ru/types/1}lastName').text if chief.find('{http://bus.gov.ru/types/1}lastName') is not None else ''
            first_name = chief.find('{http://bus.gov.ru/types/1}firstName').text if chief.find('{http://bus.gov.ru/types/1}firstName') is not None else ''
            middle_name = chief.find('{http://bus.gov.ru/types/1}middleName').text if chief.find('{http://bus.gov.ru/types/1}middleName') is not None else ''
            chief_name = last_name+' '+first_name+' '+middle_name
            chief_position = chief.find('{http://bus.gov.ru/types/1}position').text if chief.find('{http://bus.gov.ru/types/1}position') is not None else ''
            companies_df.loc[ len(companies_df.index )] = [position_id, inn, full_name, short_name, full_name_initiator, okfs_name,
                                                            okopf_name, oktmo_name, type_name,
                                                            www, eMail, phone, chief_name,
                                                                chief_position]
            global k, part
            if k >= max_rows_per_excel:
                global part
                save_to_excel(companies_df, f'companies_info_{part}.xlsx')
                part =part+1
                k=0
                companies_df = pd.DataFrame(columns=['position_id' ,'inn', 'Полное название', 'Краткое название', 'Учредитель','ОКФС', 'ОКОПФ','ОКТМО','Тип','www','eMail','phone','chief','chief_position'])

    except Exception as e:
        logging.error(f"An error occurred while parsing {xml_file_path}: {str(e)}")

    return companies_df

def save_to_excel(companies_df:pd.DataFrame, excel_file_name):
    try:
        companies_df.to_excel(excel_file_name, index=False)
        logging.info(f'File {excel_file_name} saved succefull')
    except Exception as e:
        logging.error(f"An error occurred while saving to Excel: {str(e)}")
        print(f"An error occurred while saving to Excel: {str(e)}")

def main():
    global part
    # Параметры FTP-сервера
    ftp_host = 'ftp.bus.gov.ru'
    ftp_user = 'gmuext'
    ftp_pass = 'YctTa34AdOPyld2'
    remote_directory = 'GeneralInfo'

    # Локальная директория для сохранения архивов и .xml файлов
    local_directory = 'GeneralInfo_files'

    # Создание локальной директории, если она не существует
    if not os.path.exists(local_directory):
        os.makedirs(local_directory)

    # Создание пустого DataFrame для хранения данных
    companies_df = pd.DataFrame(columns=['position_id' ,'inn', 'Полное название', 'Краткое название', 'Учредитель','ОКФС', 'ОКОПФ','ОКТМО','Тип','www','eMail','phone','chief','chief_position'])

    # Максимальное количество строк в Excel файле
    max_rows_per_excel = 50000

    try:
        # Подключение к FTP-серверу
        with FTP(ftp_host) as ftp:
            ftp.login(user=ftp_user, passwd=ftp_pass)
            print('I`m in')
            # Скачивание и извлечение .xml файлов из архивов
            companies_df = download_and_extract_xml_files(ftp, remote_directory, local_directory, companies_df, max_rows_per_excel)

        # Сохранение оставшихся данных в Excel файл
        if not companies_df.empty:
            save_to_excel(companies_df, 'companies_info_final.xlsx')

    except KeyboardInterrupt:
        save_to_excel(companies_df, f'companies_info_{part+1}.xlsx')
    except Exception as e:
        save_to_excel(companies_df, f'companies_info_{part}.xlsx')
        print(f"An error occurred during FTP connection: {str(e)}")

if __name__ == "__main__":
    main()
